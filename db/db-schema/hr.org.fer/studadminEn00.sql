CREATE DATABASE studadminEn00
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'hr_HR.UTF-8'
       LC_CTYPE = 'hr_HR.UTF-8'
       CONNECTION LIMIT = -1
        TEMPLATE template0;
GRANT ALL ON DATABASE studadminEn00 TO postgres;
GRANT ALL ON DATABASE studadminEn00 TO pgrunner;


 -- select db studadminEn00 
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: audit; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA audit;


ALTER SCHEMA audit OWNER TO postgres;

--
-- Name: SCHEMA audit; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA audit IS 'Out-of-table audit/history logging tables and trigger functions';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET search_path = audit, pg_catalog;

--
-- Name: audit_table(regclass); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit_table(target_table regclass) RETURNS void
    LANGUAGE sql
    AS $_$
SELECT audit.audit_table($1, BOOLEAN 't', BOOLEAN 't');
$_$;


ALTER FUNCTION audit.audit_table(target_table regclass) OWNER TO postgres;

--
-- Name: FUNCTION audit_table(target_table regclass); Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON FUNCTION audit_table(target_table regclass) IS '
Add auditing support to the given table. Row-level changes will be logged with full client query text. No cols are ignored.
';


--
-- Name: audit_table(regclass, boolean, boolean); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean) RETURNS void
    LANGUAGE sql
    AS $_$
SELECT audit.audit_table($1, $2, $3, ARRAY[]::text[]);
$_$;


ALTER FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean) OWNER TO postgres;

--
-- Name: audit_table(regclass, boolean, boolean, text[]); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  stm_targets text = 'INSERT OR UPDATE OR DELETE OR TRUNCATE';
  _q_txt text;
  _ignored_cols_snip text = '';
BEGIN
    EXECUTE 'DROP TRIGGER IF EXISTS stamp ON ' || quote_ident(target_table::TEXT); -- modified
    EXECUTE 'DROP TRIGGER IF EXISTS audit_trigger_row ON ' || quote_ident(target_table::TEXT);
    EXECUTE 'DROP TRIGGER IF EXISTS audit_trigger_stm ON ' || quote_ident(target_table::TEXT);

    IF audit_rows THEN
        IF array_length(ignored_cols,1) > 0 THEN
            _ignored_cols_snip = ', ' || quote_literal(ignored_cols);
        END IF;
        _q_txt = 'CREATE TRIGGER audit_trigger_row AFTER INSERT OR UPDATE OR DELETE ON ' ||
                 quote_ident(target_table::TEXT) ||
                 ' FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func(' ||
                 quote_literal(audit_query_text) || _ignored_cols_snip || ');';
        RAISE NOTICE '%',_q_txt;
        EXECUTE _q_txt;
        stm_targets = 'TRUNCATE';
    ELSE
    END IF;
    _q_txt = 'CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON ' ||    -- modified
             target_table ||
             ' FOR EACH ROW EXECUTE PROCEDURE audit.stamp()';
    RAISE NOTICE '%',_q_txt;
    EXECUTE _q_txt;

    _q_txt = 'CREATE TRIGGER audit_trigger_stm AFTER ' || stm_targets || ' ON ' ||
             target_table ||
             ' FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('||
             quote_literal(audit_query_text) || ');';
    RAISE NOTICE '%',_q_txt;
    EXECUTE _q_txt;

END;
$$;


ALTER FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) OWNER TO postgres;

--
-- Name: FUNCTION audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]); Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON FUNCTION audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) IS '
Add auditing support to a table.

Arguments:
   target_table:     Table name, schema qualified if not on search_path
   audit_rows:       Record each row change, or only audit at a statement level
   audit_query_text: Record the text of the client query that triggered the audit event?
   ignored_cols:     Columns to exclude from update diffs, ignore updates that change only ignored cols.
';


--
-- Name: if_modified_func(); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION if_modified_func() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO pg_catalog, public
    AS $$
DECLARE
    audit_row audit.logged_actions;
    include_values BOOLEAN;
    log_diffs BOOLEAN;
    h_old hstore;
    h_new hstore;
    excluded_cols text[] = ARRAY[]::text[];
BEGIN
    IF TG_WHEN <> 'AFTER' THEN
        RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
    END IF;

    audit_row = ROW(
        nextval('audit.logged_actions_event_id_seq'), -- event_id
        TG_TABLE_SCHEMA::text,                        -- schema_name
        TG_TABLE_NAME::text,                          -- table_name
        TG_RELID,                                     -- relation OID for much quicker searches
        session_user::text,                           -- session_user_name
        current_timestamp,                            -- action_tstamp_tx
        statement_timestamp(),                        -- action_tstamp_stm
        clock_timestamp(),                            -- action_tstamp_clk
        txid_current(),                               -- transaction ID
        current_setting('application_name'),          -- client application
        inet_client_addr(),                           -- client_addr
        inet_client_port(),                           -- client_port
        current_query(),                              -- top-level query or queries (if multistatement) from client
        substring(TG_OP,1,1),                         -- action
        NULL, NULL,                                   -- row_data, changed_fields
        'f'                                           -- statement_only
        );

    IF NOT TG_ARGV[0]::BOOLEAN IS DISTINCT FROM 'f'::BOOLEAN THEN
        audit_row.client_query = NULL;
    END IF;

    IF TG_ARGV[1] IS NOT NULL THEN
        excluded_cols = TG_ARGV[1]::text[];
    END IF;

    IF (TG_OP = 'UPDATE' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(OLD.*) - excluded_cols;
        audit_row.changed_fields =  (hstore(NEW.*) - audit_row.row_data) - excluded_cols;
        IF audit_row.changed_fields = hstore('') THEN
            -- All changed fields are ignored. Skip this update.
            RETURN NULL;
        END IF;
    ELSIF (TG_OP = 'DELETE' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(OLD.*) - excluded_cols;
    ELSIF (TG_OP = 'INSERT' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(NEW.*) - excluded_cols;
    ELSIF (TG_LEVEL = 'STATEMENT' AND TG_OP IN ('INSERT','UPDATE','DELETE','TRUNCATE')) THEN
        audit_row.statement_only = 't';
    ELSE
        RAISE EXCEPTION '[audit.if_modified_func] - Trigger func added as trigger for unhandled case: %, %',TG_OP, TG_LEVEL;
        RETURN NULL;
    END IF;
    INSERT INTO audit.logged_actions VALUES (audit_row.*);
    RETURN NULL;
END;
$$;


ALTER FUNCTION audit.if_modified_func() OWNER TO postgres;

--
-- Name: FUNCTION if_modified_func(); Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON FUNCTION if_modified_func() IS '
Track changes to a table at the statement and/or row level.

Optional parameters to trigger in CREATE TRIGGER call:

param 0: BOOLEAN, whether to log the query text. Default ''t''.

param 1: text[], columns to ignore in updates. Default [].

         Updates to ignored cols are omitted from changed_fields.

         Updates with only ignored cols changed are not inserted
         into the audit log.

         Almost all the processing work is still done for updates
         that ignored. If you need to save the load, you need to use
         WHEN clause on the trigger instead.

         No warning or error is issued if ignored_cols contains columns
         that do not exist in the target table. This lets you specify
         a standard set of ignored columns.

There is no parameter to disable logging of values. Add this trigger as
a ''FOR EACH STATEMENT'' rather than ''FOR EACH ROW'' trigger if you do not
want to log row values.

Note that the user name logged is the login role for the session. The audit trigger
cannot obtain the active role because it is reset by the SECURITY DEFINER invocation
of the audit trigger its self.
';


--
-- Name: stamp(); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION stamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
    
        RETURN NEW;
    END;
$$;


ALTER FUNCTION audit.stamp() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: logged_actions; Type: TABLE; Schema: audit; Owner: postgres
--

CREATE TABLE logged_actions (
    event_id bigint NOT NULL,
    schema_name text NOT NULL,
    table_name text NOT NULL,
    relid oid NOT NULL,
    session_user_name text,
    action_tstamp_tx timestamp with time zone NOT NULL,
    action_tstamp_stm timestamp with time zone NOT NULL,
    action_tstamp_clk timestamp with time zone NOT NULL,
    transaction_id bigint,
    application_name text,
    client_addr inet,
    client_port integer,
    client_query text,
    action text NOT NULL,
    row_data public.hstore,
    changed_fields public.hstore,
    statement_only boolean NOT NULL,
    CONSTRAINT logged_actions_action_check CHECK ((action = ANY (ARRAY['I'::text, 'D'::text, 'U'::text, 'T'::text])))
);


ALTER TABLE logged_actions OWNER TO postgres;

--
-- Name: TABLE logged_actions; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON TABLE logged_actions IS 'History of auditable actions on audited tables, from audit.if_modified_func()';


--
-- Name: COLUMN logged_actions.event_id; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.event_id IS 'Unique identifier for each auditable event';


--
-- Name: COLUMN logged_actions.schema_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.schema_name IS 'Database schema audited table for this event is in';


--
-- Name: COLUMN logged_actions.table_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.table_name IS 'Non-schema-qualified table name of table event occured in';


--
-- Name: COLUMN logged_actions.relid; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.relid IS 'Table OID. Changes with drop/create. Get with ''tablename''::regclass';


--
-- Name: COLUMN logged_actions.session_user_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.session_user_name IS 'Login / session user whose statement caused the audited event';


--
-- Name: COLUMN logged_actions.action_tstamp_tx; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.action_tstamp_tx IS 'Transaction start timestamp for tx in which audited event occurred';


--
-- Name: COLUMN logged_actions.action_tstamp_stm; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.action_tstamp_stm IS 'Statement start timestamp for tx in which audited event occurred';


--
-- Name: COLUMN logged_actions.action_tstamp_clk; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.action_tstamp_clk IS 'Wall clock time at which audited event''s trigger call occurred';


--
-- Name: COLUMN logged_actions.transaction_id; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.transaction_id IS 'Identifier of transaction that made the change. May wrap, but unique paired with action_tstamp_tx.';


--
-- Name: COLUMN logged_actions.application_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.application_name IS 'Application name set when this audit event occurred. Can be changed in-session by client.';


--
-- Name: COLUMN logged_actions.client_addr; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.client_addr IS 'IP address of client that issued query. Null for unix domain socket.';


--
-- Name: COLUMN logged_actions.client_port; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.client_port IS 'Remote peer IP port address of client that issued query. Undefined for unix socket.';


--
-- Name: COLUMN logged_actions.client_query; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.client_query IS 'Top-level query that caused this auditable event. May be more than one statement.';


--
-- Name: COLUMN logged_actions.action; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.action IS 'Action type; I = insert, D = delete, U = update, T = truncate';


--
-- Name: COLUMN logged_actions.row_data; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.row_data IS 'Record value. Null for statement-level trigger. For INSERT this is the new tuple. For DELETE and UPDATE it is the old tuple.';


--
-- Name: COLUMN logged_actions.changed_fields; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.changed_fields IS 'New values of fields changed by UPDATE. Null except for row-level UPDATE events.';


--
-- Name: COLUMN logged_actions.statement_only; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN logged_actions.statement_only IS '''t'' if audit event is from an FOR EACH STATEMENT trigger, ''f'' for FOR EACH ROW';


--
-- Name: logged_actions_event_id_seq; Type: SEQUENCE; Schema: audit; Owner: postgres
--

CREATE SEQUENCE logged_actions_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE logged_actions_event_id_seq OWNER TO postgres;

--
-- Name: logged_actions_event_id_seq; Type: SEQUENCE OWNED BY; Schema: audit; Owner: postgres
--

ALTER SEQUENCE logged_actions_event_id_seq OWNED BY logged_actions.event_id;


SET search_path = public, pg_catalog;



--
-- Name: edgar_indexes; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW edgar_indexes AS
 SELECT ct.relname AS table_name,
    i.indisunique,
    ci.relname AS index_name,
    (i.keys).n AS ordinal_position,
    pg_get_indexdef(ci.oid, (i.keys).n, false) AS column_name,
        CASE ((i.indoption[((i.keys).n - 1)])::integer & 1)
            WHEN 1 THEN 'DESC'::text
            ELSE 'ASC'::text
        END AS direction,
    pg_get_expr(i.indpred, i.indrelid) AS filter_condition
   FROM ((((pg_class ct
     JOIN pg_namespace n ON ((ct.relnamespace = n.oid)))
     JOIN ( SELECT i_1.indexrelid,
            i_1.indrelid,
            i_1.indoption,
            i_1.indisunique,
            i_1.indisclustered,
            i_1.indpred,
            i_1.indexprs,
            information_schema._pg_expandarray(i_1.indkey) AS keys
           FROM pg_index i_1) i ON ((ct.oid = i.indrelid)))
     JOIN pg_class ci ON ((ci.oid = i.indexrelid)))
     JOIN pg_am am ON ((ci.relam = am.oid)));


ALTER TABLE edgar_indexes OWNER TO postgres;

--
-- Name: edgar_indexes_all; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW edgar_indexes_all AS
 SELECT edgar_indexes.table_name,
    array_to_string(array_agg((edgar_indexes.column_name || edgar_indexes.direction) ORDER BY edgar_indexes.ordinal_position), ','::text) AS array_agg
   FROM edgar_indexes
  GROUP BY edgar_indexes.index_name, edgar_indexes.table_name
UNION ALL
 SELECT edgar_indexes.table_name,
    array_to_string(array_agg((edgar_indexes.column_name ||
        CASE
            WHEN (edgar_indexes.direction = 'ASC'::text) THEN 'DESC'::text
            ELSE 'ASC'::text
        END) ORDER BY edgar_indexes.ordinal_position), ','::text) AS array_agg
   FROM edgar_indexes
  GROUP BY edgar_indexes.index_name, edgar_indexes.table_name;


ALTER TABLE edgar_indexes_all OWNER TO postgres;

--
-- Name: edgar_keys; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW edgar_keys AS
 SELECT DISTINCT tc.table_name,
    tc.constraint_type,
    'NULL'::text AS refd_table_name,
    array_to_string(array_agg(((kcu.column_name)::text || ' '::text) ORDER BY kcu.column_name), ','::text) AS key_def
   FROM (information_schema.table_constraints tc
     JOIN information_schema.key_column_usage kcu ON ((((tc.table_name)::text = (kcu.table_name)::text) AND ((tc.constraint_type)::text = 'PRIMARY KEY'::text))))
  GROUP BY tc.table_name, tc.constraint_type, 'NULL'::text
UNION ALL
 SELECT t1.table_name,
    t1.constraint_type,
    t1.refd_table_name,
    array_to_string(array_agg((((tfk.attname)::text || '-'::text) || (trefd.attname)::text) ORDER BY trefd.attname), ','::text) AS key_def
   FROM ((( SELECT DISTINCT tc.table_name,
            tc.constraint_type,
            'NULL'::text AS column_name,
            ccu.table_name AS refd_table_name,
            pc.conname,
            unnest(pc.conkey) AS col_num,
            unnest(pc.confkey) AS ref_col_num
           FROM ((information_schema.table_constraints tc
             JOIN information_schema.constraint_column_usage ccu ON ((((ccu.constraint_name)::text = (tc.constraint_name)::text) AND ((tc.constraint_type)::text = 'FOREIGN KEY'::text))))
             JOIN pg_constraint pc ON (((tc.constraint_name)::name = pc.conname)))) t1
     JOIN ( SELECT c.relname,
            a.attname,
            a.attnum
           FROM ((pg_class c
             JOIN pg_attribute a ON (((a.attnum > 0) AND (a.attrelid = c.oid))))
             JOIN pg_type t ON ((a.atttypid = t.oid)))) tfk ON ((((t1.table_name)::name = tfk.relname) AND (t1.col_num = tfk.attnum))))
     JOIN ( SELECT c.relname,
            a.attname,
            a.attnum
           FROM ((pg_class c
             JOIN pg_attribute a ON (((a.attnum > 0) AND (a.attrelid = c.oid))))
             JOIN pg_type t ON ((a.atttypid = t.oid)))) trefd ON ((((t1.refd_table_name)::name = trefd.relname) AND (t1.ref_col_num = trefd.attnum))))
  GROUP BY t1.table_name, t1.constraint_type, t1.refd_table_name, t1.conname;


ALTER TABLE edgar_keys OWNER TO postgres;

SET search_path = audit, pg_catalog;

--
-- Name: logged_actions event_id; Type: DEFAULT; Schema: audit; Owner: postgres
--

ALTER TABLE ONLY logged_actions ALTER COLUMN event_id SET DEFAULT nextval('logged_actions_event_id_seq'::regclass);


--
-- Name: logged_actions logged_actions_pkey; Type: CONSTRAINT; Schema: audit; Owner: postgres
--

ALTER TABLE ONLY logged_actions
    ADD CONSTRAINT logged_actions_pkey PRIMARY KEY (event_id);


--
-- Name: logged_actions_action_idx; Type: INDEX; Schema: audit; Owner: postgres
--

CREATE INDEX logged_actions_action_idx ON logged_actions USING btree (action);


--
-- Name: logged_actions_action_tstamp_tx_stm_idx; Type: INDEX; Schema: audit; Owner: postgres
--

CREATE INDEX logged_actions_action_tstamp_tx_stm_idx ON logged_actions USING btree (action_tstamp_stm);


--
-- Name: logged_actions_relid_idx; Type: INDEX; Schema: audit; Owner: postgres
--

CREATE INDEX logged_actions_relid_idx ON logged_actions USING btree (relid);


SET search_path = public, pg_catalog;

















-- *************************************************************************************************************
-- *************************************************************************************************************
-- *************************************************************************************************************
-- *************************************************************************************************************
-- *************************************************************************************************************
-- ******************************** Dalje ide promjenjivi dio, baza koja se svake godine ažurira ***************
-- *************************************************************************************************************
-- *************************************************************************************************************
-- *************************************************************************************************************
-- *************************************************************************************************************
-- *************************************************************************************************************



-- (...)



-- *************************************************************************************************************
-- ******************************** Nakon toga, obaviti, pa C/P, pa obaviti: 					 ***************
-- *************************************************************************************************************

SELECT 'CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON ' || tablename || ' FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func(''true'');'
FROM pg_tables
WHERE schemaname = 'public';


SELECT 'CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON ' || tablename || ' FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func(''true'');'
FROM pg_tables
WHERE schemaname = 'public';


SELECT 'CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON ' || tablename || ' FOR EACH ROW EXECUTE PROCEDURE audit.stamp();'
FROM pg_tables
WHERE schemaname = 'public';



-- *************************************************************************************************************
-- ******************************** ... and you're golden.                  					 ***************
-- *************************************************************************************************************

SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'studadminen00'
  AND pid <> pg_backend_pid();


drop database studadminEn01;
drop database studadminEn02;
drop database studadminEn03;
drop database studadminEn04;
drop database studadminEn05;
drop database studadminEn06;




CREATE DATABASE studadminEn01
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'hr_HR.UTF-8'
       LC_CTYPE = 'hr_HR.UTF-8'
       CONNECTION LIMIT = -1
        TEMPLATE studadminEn00;
GRANT ALL ON DATABASE studadminEn01 TO postgres;
GRANT ALL ON DATABASE studadminEn01 TO pgrunner;


CREATE DATABASE studadminEn02
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'hr_HR.UTF-8'
       LC_CTYPE = 'hr_HR.UTF-8'
       CONNECTION LIMIT = -1
        TEMPLATE studadminEn00;
GRANT ALL ON DATABASE studadminEn02 TO postgres;
GRANT ALL ON DATABASE studadminEn02 TO pgrunner;


CREATE DATABASE studadminEn03
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'hr_HR.UTF-8'
       LC_CTYPE = 'hr_HR.UTF-8'
       CONNECTION LIMIT = -1
        TEMPLATE studadminEn00;
GRANT ALL ON DATABASE studadminEn03 TO postgres;
GRANT ALL ON DATABASE studadminEn03 TO pgrunner;


CREATE DATABASE studadminEn04
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'hr_HR.UTF-8'
       LC_CTYPE = 'hr_HR.UTF-8'
       CONNECTION LIMIT = -1
        TEMPLATE studadminEn00;
GRANT ALL ON DATABASE studadminEn04 TO postgres;
GRANT ALL ON DATABASE studadminEn04 TO pgrunner;


CREATE DATABASE studadminEn05
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'hr_HR.UTF-8'
       LC_CTYPE = 'hr_HR.UTF-8'
       CONNECTION LIMIT = -1
        TEMPLATE studadminEn00;
GRANT ALL ON DATABASE studadminEn05 TO postgres;
GRANT ALL ON DATABASE studadminEn05 TO pgrunner;


CREATE DATABASE studadminEn06
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'hr_HR.UTF-8'
       LC_CTYPE = 'hr_HR.UTF-8'
       CONNECTION LIMIT = -1
        TEMPLATE studadminEn00;
GRANT ALL ON DATABASE studadminEn06 TO postgres;
GRANT ALL ON DATABASE studadminEn06 TO pgrunner;





\c studadminen01
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO pgrunner;
\c studadminen02
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO pgrunner;
\c studadminen03
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO pgrunner;
\c studadminen04
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO pgrunner;
\c studadminen05
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO pgrunner;
\c studadminen06
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO pgrunner;