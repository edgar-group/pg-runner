#!/usr/bin/env bash
# https://docs.yugabyte.com/preview/sample-data/chinook
files=(
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/chinook_ddl.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/chinook_genres_artists_albums.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/chinook_songs.sql"
)
for file in "${files[@]}"; do
    echo "--- $file ---"
    ( echo "CREATE SCHEMA IF NOT EXISTS chinook; SET search_path TO chinook;" & curl "$file" ) | cat | psql -U postgres -d examdb
    echo; echo
done

# https://docs.yugabyte.com/preview/sample-data/sportsdb
files=(
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/sportsdb_tables.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/sportsdb_inserts.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/sportsdb_constraints.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/sportsdb_fks.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/sportsdb_indexes.sql"
)
for file in "${files[@]}"; do
    echo "--- $file ---"
    ( echo "CREATE SCHEMA IF NOT EXISTS sportsdb; SET search_path TO sportsdb;" & curl "$file" ) | cat | psql -U postgres -d examdb
    echo; echo
done

# https://docs.yugabyte.com/preview/sample-data/retail-analytics
files=(
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/schema.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/orders.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/products.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/reviews.sql"
    "https://raw.githubusercontent.com/yugabyte/yugabyte-db/master/sample/users.sql"
)
for file in "${files[@]}"; do
    echo "--- $file ---"
    ( echo "CREATE SCHEMA IF NOT EXISTS retail_analytics; SET search_path TO retail_analytics;" & curl "$file" ) | cat | psql -U postgres -d examdb
    echo; echo
done
